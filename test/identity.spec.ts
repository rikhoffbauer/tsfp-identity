import identity from "../src";

describe(`@tsfp/identity`, () => {
    it(`should return the value passed in`, async () => {
        const val = {};

        expect(identity(val)).toBe(val);
    });
});
