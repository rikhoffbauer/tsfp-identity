# @tsfp/identity
![coverage](https://gitlab.com/rikhoffbauer/tsfp-identity/badges/master/coverage.svg)
![build](https://gitlab.com/rikhoffbauer/tsfp-identity/badges/master/build.svg)
![npm version](https://badge.fury.io/js/%40tsfp%2Fidentity.svg)

An identity function written in TypeScript.

## Installing

##### Using npm

`npm install @tsfp/identity`

##### Using yarn

`yarn add @tsfp/identity`

## Usage

```typescript
import identity from "@tsfp/identity";
const a = {};
identity(a) === a; // true
```

For more information see the [API documentation](https://rikhoffbauer.gitlab.io/tsfp-identity).

## Contributing

Contributions in the form of pull requests and bug reports are appreciated.

### Running tests

Tests are ran using the `test` npm script.

##### Using npm

`npm test`

##### Using yarn

`yarn test`
