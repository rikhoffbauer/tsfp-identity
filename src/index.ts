/**
 * `identity` returns the value passed in.
 * ### Example
 *  ```
 * import identity from "@tsfp/identity";
 * const a = {};
 * identity(a) === a; // true
 *  ```
 */
const identity = <T>(value: T): T => value;

export default identity;
